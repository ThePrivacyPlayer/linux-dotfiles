#My Shell Startup Script

#VISUAL APPEARANCE
redshift -l 50.85045:4.34878 -t 5500:3700 &
nitrogen --restore & #Background
compton --config ~/.config/compton/compton.conf & #Compositor
plank &

guake &	#Drop-down terminal

trayer --edge top --align right --SetDockType true --SetPartialStrut false --expand true --transparent true --alpha 0 --tint 0x000000 --width 6 --height 22 &
